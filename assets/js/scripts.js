new Chartist.Pie('.ct-chart1', {
  series: [20, 80]
}, {
  donut: true,
  donutWidth: 20,
  donutSolid: true,
	startAngle: 270,
  showLabel: false,
});

new Chartist.Pie('.ct-chart2', {
  series: [50, 50]
}, {
  donut: true,
  donutWidth: 20,
  donutSolid: true,
	startAngle: 180,
  showLabel: false,
});

new Chartist.Pie('.ct-chart3', {
  series: [40, 60]
}, {
  donut: true,
  donutWidth: 20,
  donutSolid: true,
	startAngle: 230,
  showLabel: false,
});

new Chartist.Bar('.ct-chartLine', {
  // labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  series: [
    [8, 0, 0, 0, 0],
    [0, 6, 0, 0, 0],
    [0, 0, 7, 0, 0],
    [0, 0, 0, 9, 0],
    [0, 0, 0, 0, 9],
  ]
}, {
  seriesBarDistance: 10,
  reverseData: true,
	horizontalBars: true,
	showLabel: false,
  axisY: {
    offset: 70
  }
});

new Chartist.Line('.ct-chartSimple', {
  labels: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
  series: [
    [0, 6, 5, 15, 5, 1, 0],
    [0, 6, 5, 15]
  ]
}, {
	fullWidth: true,
	showArea: true,
  chartPadding: {
    right: 40
  }
});
